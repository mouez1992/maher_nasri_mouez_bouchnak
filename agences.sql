-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 18 Février 2015 à 12:37
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `agences`
--

-- --------------------------------------------------------

--
-- Structure de la table `aeroport`
--

CREATE TABLE IF NOT EXISTS `aeroport` (
  `code_aeroport` varchar(3) NOT NULL,
  `nom_aeroport` varchar(200) NOT NULL,
  `ville_aeroport` varchar(100) NOT NULL,
  `pays_aeroport` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`code_aeroport`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `appareil`
--

CREATE TABLE IF NOT EXISTS `appareil` (
  `id_appareil` int(11) NOT NULL,
  `marque_appareil` varchar(45) DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_appareil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `compagnie`
--

CREATE TABLE IF NOT EXISTS `compagnie` (
  `id_compagnie` int(10) NOT NULL,
  `nom_compagnie` varchar(45) NOT NULL,
  PRIMARY KEY (`id_compagnie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `vol`
--

CREATE TABLE IF NOT EXISTS `vol` (
  `code_vol` varchar(5) NOT NULL,
  `id_compagnie` int(10) DEFAULT NULL,
  `date_depart_vol` date NOT NULL,
  `date_arrivé_vol` date NOT NULL,
  `code_aeroport_depart` varchar(3) DEFAULT NULL,
  `code_aeroport_arrivé` varchar(3) DEFAULT NULL,
  `id_appareil` int(11) DEFAULT NULL,
  PRIMARY KEY (`code_vol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
